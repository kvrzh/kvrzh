<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="css/styles.css" rel="stylesheet">
<script src="js/jquery-2.2.2.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<link rel='stylesheet' href='css/bootstrap.min.css' type='text/css' media='all'>


<?php

if (isset($_GET['add']))
{
    include 'pages/forms.html.php';
    exit();
}

try
{
    $pdo = new PDO('mysql:host=localhost;dbname=films', 'films', 'films');
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->exec('SET NAMES "utf8"');
}
catch (PDOException $e)
{
    $error = 'Unable to connect to the database server.';
    include 'error.html.php';
    exit();
}

if (isset($_POST['name']))
{
    try
    {
        $sql = 'INSERT INTO films SET
        name = :name,
        year = :year,
        isActive = :isActive';
        if($_POST['isActive']==1){
            $isactive = 1;
        } else{
            $isactive = 0;
        }
        $s = $pdo->prepare($sql);
        $s->bindValue(':name', $_POST['name']);
        $s->bindValue(':year', $_POST['year']);
        $s->bindValue(':isActive', $isactive);
        $s->execute();
    }
    catch (PDOException $e)
    {
        $error = 'Error adding submitted joke: ' . $e->getMessage();
        include 'include/error.html.php';
        exit();
    }

    header('Location: .');
    exit();
}



try
{
    $sql = 'SELECT id, name, year FROM films WHERE isActive=1';
    $result = $pdo->query($sql);
}
catch (PDOException $e)
{
    $error = 'Error fetching jokes: ' . $e->getMessage();
    include 'error.html.php';
    exit();
}

while ($row = $result->fetch())
{
    $films[] = array('id' => $row['id'], 'name' => $row['name'], 'year' =>$row['year']);
}

include 'pages/films.html.php';




?>