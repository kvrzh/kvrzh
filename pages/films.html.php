<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="utf-8">
    <title>List of films</title>
</head>
<body>

<h2 class="col-md-6 col-md-offset-3">All the films what have 'isActive' mark:</h2>

    <div class="col-md-8  col-md-offset-2 ">
        <table class="table">
            <thead>
            <tr>
                <th>ID</th>
                <th>Имя</th>
                <th>Год</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($films as $film): ?>
            <tr>
                <td><?= $film['id'] ?></td>
                <td><?= $film['name'] ?></td>
                <td><?= $film['year'] ?></td>
            </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<div class="col-md-6 col-md-offset-3">
<a class="btn btn-success btn-block" href="?add" role="button">Добавить фильм</a>
</div>


</body>
</html>
