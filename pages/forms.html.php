<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Add Joke</title>
    <style type="text/css">
        textarea {
            display: block;
            width: 100%;
        }
    </style>
</head>
<body>
<div class="col-md-6 col-md-offset-3">
<form role="form" action="?" method="post">
    <div class="form-group">
        <label for="name"><h3 class="text-center">Name</h3></label>
        <input type="text" class="form-control" id="name" name="name" placeholder="Enter the name of film">

    </div>
    <div class="form-group">
        <label for="year"><h3 class="text-center">Year</h3></label>
        <input type="text" class="form-control" id="year" name="year" placeholder="Enter the year">
        <p class="help-block">Example: 1994</p>
    </div>
    <div class="checkbox">
        <label><input type="checkbox" name="isActive" value="1"> isActive</label>
    </div>
    <input class="btn btn-success btn-block" type="submit" value="Add">
</form>
</div>
</body>
</html>
